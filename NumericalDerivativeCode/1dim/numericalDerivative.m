function [ basis_vector, add_vector ] = numericalDerivative( v, degree,...
    a, b, p, ...
    element_position,num_of_elements,...
    derivative_type, accuracy,...
    polynomial_values, polynomial_int_values, polynomial_deriv_values,...
    v_at_a, v_at_b)
%Computes numerical derivative a specified domain

%%% AUTHOR: Stefan Schnake, 2014

%Call forth mass matrix and b_vector
%A = createMassMatrix(degree);
[plus_b_vector, minus_b_vector] = createRightHandSide(v,degree,a,b,p,...
    element_position,num_of_elements,accuracy,...
    polynomial_values, polynomial_int_values, polynomial_deriv_values,...
    v_at_a, v_at_b);

%Calculate answer
if strcmp(derivative_type,'full')
    basis_vector = (2/(b-a))*(1/2)*(plus_b_vector + minus_b_vector);
elseif strcmp(derivative_type,'plus')
    basis_vector = (2/(b-a))*plus_b_vector;
elseif strcmp(derivative_type,'minus')
    basis_vector = (2/(b-a))*minus_b_vector;
elseif strcmp(derivative_type,'both')
    basis_vector = (2/(b-a))*plus_b_vector;
    add_vector = (2/(b-a))*minus_b_vector;
end

%we just need to scale
%the vector by (2/(b-a)) from the affine transformation.

end

