function [ poly_data ] = L2proj( v,degree,p )
#Creates the local L2 projection of the function v.

%%% AUTHOR: Stefan Schnake, 2014

normalization_vector = [1/2; 3/2; 5/2; 7/2; 9/2; 11/2];
normalization = normalization_vector(1:degree+1);

%Guassian quadrature points
point_vector = [-0.1488743389816312;
0.1488743389816312;
-0.4333953941292472;
0.4333953941292472;
-0.6794095682990244;
0.6794095682990244;
-0.8650633666889845;
0.8650633666889845;
-0.9739065285171717;
0.9739065285171717]';

weight_vector = [0.2955242247147529;
0.2955242247147529;
0.2692667193099963;
0.2692667193099963;
0.2190863625159820;
0.2190863625159820;
0.1494513491505806;
0.1494513491505806;
0.0666713443086881;
0.0666713443086881]';

%Legendre Polynomials
polys = {@(x) ones(size(x)),@(x) x, @(x) (1/2)*(3*x.^2-1), @(x) (1/2)*(5*x.^3-3*x)};

poly_data = zeros(degree+1,size(p,2)-1);

for i = 1:size(p,2)-1
    b = zeros(degree+1,1);
    midpoint = (1/2)*(p(i+1)-p(i));
    average = (1/2)*(p(i+1)+p(i));
    for j = 1:degree+1
        obj = @(x) v(midpoint*x+average).*polys{j}(x);
        b(j) = integral(obj,-1,1);
    end
    poly_data(:,i) = b.*normalization;
end


end

