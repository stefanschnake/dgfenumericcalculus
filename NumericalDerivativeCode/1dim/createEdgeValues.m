function [ edge_values ] = createEdgeValues( original_v, p, polynomial_values )
%CREATEEDGEVALUES 

%%% AUTHOR: Stefan Schnake, 2014

%Defining the +- operators for the boundary integral
%Note that n_e = 1 for any edge in the 1D case
if isa(original_v,'double')
    number_of_elements = size(original_v,2);
    a = repmat(polynomial_values(:,1),1,number_of_elements);
    b = repmat(polynomial_values(:,2),1,number_of_elements);
    left = sum(a.*original_v,1);
    right = sum(b.*original_v,1);
    edge_values = [left right(end);left(1) right];     
else
    edge_values = original_v(p);
    if size(edge_values,1) == 1
        edge_values = repmat(edge_values,2,1);
    else
        
end

end

