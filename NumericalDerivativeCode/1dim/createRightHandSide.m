function [ plus_b_vector, minus_b_vector ] = createRightHandSide(  ...
    original_v, degree,a,b,p,element_position,num_of_elements,accuracy,...
    polynomial_values, polynomial_int_values, polynomial_deriv_values,...
    v_at_a, v_at_b)

%%% AUTHOR: Stefan Schnake, 2014

%Create Right Hand Side of the algorithm
number_of_basis = 1+degree;

%Gaussian quadrature of the function on [-1,1] using multiple points
%See page 225 of "Numerical Analysis", 
%Seventh Ed by Burden and Faires for more information



%The Legendre basis is orthogonal but not orthonormal.  This would create a
%diagonal matrix on the left hand side with the values below on the
%diagonal.
normalization_vector = [1/2; 3/2; 5/2; 7/2; 9/2; 11/2];
%Scale normalization vector to proper height
normalization = normalization_vector(1:number_of_basis,1);

%If v is previous derivative data, then we need to convert it from the
%simplex [-1,1] to the domain [a,b].  First we define the function v, then
%overwrite it if original_v is a function.
if isa(original_v,'double') == 0
    %v = @(x) postProcessing(original_v,p,0,x);
    v = original_v;
end

%Create Q+- operators
N = size(v_at_a,1);
if N>=2;
    avg_of_v_at_a = (1/2)*(v_at_a(1)+v_at_a(2));
    jump_of_v_at_a = v_at_a(1) - v_at_a(2); 
else
    avg_of_v_at_a = v_at_a;
    jump_of_v_at_a = 0;
end
N = size(v_at_b,1);
if N>=2;
    avg_of_v_at_b = (1/2)*(v_at_b(1)+v_at_b(2));
    jump_of_v_at_b = v_at_b(1) - v_at_b(2); 
else
    avg_of_v_at_b = v_at_b;
    jump_of_v_at_b = 0;
end
Q_plus_at_a = avg_of_v_at_a + (1/2)*jump_of_v_at_a;
Q_minus_at_a = avg_of_v_at_a - (1/2)*jump_of_v_at_a;
Q_plus_at_b = avg_of_v_at_b + (1/2)*jump_of_v_at_b;
Q_minus_at_b = avg_of_v_at_b - (1/2)*jump_of_v_at_b;

%Create a refined point matrix that maps [-1,1] to [a,b] only if we do not
%have previous data. These points will be used in the numerical integration.
[point_matrix, weight_vector] = createPointWeightVectors(accuracy);
if isa(original_v,'double') == 0
    refined_point_matrix = ((b-a).*point_matrix+(b+a))/2;
end


%Create values for v that will be used in the integration.  Since the
%quadrature rule does not use the endpoints of the subinterval we do not
%need to worry about discontinuities.
%v could have multiple values anyway by constuction of the function.  If it
%does just take the first row since both rows will be the same.
%Finally make the v_values have the same rows as the number_of_basis, and
%the plus and minus edge values by the values repeated from above.
%temp_v_values = v(refined_point_matrix);
if isa(original_v,'double')
    coefficients = repmat(original_v(:,element_position),1,size(point_matrix,2));
    temp_v_values = sum(coefficients.*polynomial_int_values);
else
    temp_v_values = v(refined_point_matrix);
end
v_values =  repmat(temp_v_values(1,:),number_of_basis,1);
plus_v_values = repmat([-Q_plus_at_a Q_plus_at_b],number_of_basis,1);
minus_v_values = repmat([-Q_minus_at_a Q_minus_at_b],number_of_basis,1);



%Now that we have the values we can compute the right hand side
weight_matrix =  repmat(weight_vector,number_of_basis,1);
gauss_integral = sum(weight_matrix.*v_values.*polynomial_deriv_values,2);
plus_edge_integral = sum(plus_v_values.*polynomial_values,2);
minus_edge_integral = sum(minus_v_values.*polynomial_values,2);
plus_b_vector = normalization.*(plus_edge_integral - gauss_integral);
minus_b_vector = normalization.*(minus_edge_integral - gauss_integral);

end
