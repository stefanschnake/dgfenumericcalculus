function [ poly_data, add_data ] = meshNumericalDerivative( v_input, degree, p, varargin)
%Computes numerical derivative on mesh

%%% AUTHOR: Stefan Schnake, 2014

%p : numerical mesh
%degree : degree of polynomial basis
%v_string : string of DG function

%Get row and column size of p
[column_size] = size(p,2);
%number_of_basis = nchoosek(1+degree,degree);
number_of_basis = 1+degree;

%Parsing variable argument options
%Setting defaults
derivative_type = 'full';
accuracy = 'vhigh';

%The function must have at least 3 agruments
if nargin < 3
    error('MATLAB:RequiredArguments','This function must have at least three arguments')
elseif mod(nargin,2) == 0 %The function must have an odd number of inputs 
    error('MATLAB:RequiredArguments','Every optional arguemnt must have a string identifier with it.')
elseif nargin > 3
    %Handling extra outputs
    for i = 4:2:nargin
        place = i-3;
        if strcmp(varargin{place},'piecewise_constants_plus')
            %Check if they are the same size at p
            if all(size(p) == size(varargin{place+1}))
                piecewise_boundary_constants_plus = varargin{place+1};
            else
                error('MATLAB:DimensionMismatch','piecewise_constants_plus is the the same size as p.');
            end
        elseif strcmp(varargin{place},'piecewise_constants_minus')
            if all(size(p) == size(varargin{place+1}))
                piecewise_boundary_constants_minus = varargin{place+1};
            else
                error('MATLAB:DimensionMismatch','piecewise_constants_plus is the the same size as p.');
            end
        elseif strcmp(varargin{place},'derivative')
            if any(validatestring(varargin{place+1},{'plus','minus','full','both'}));
                derivative_type = varargin{place+1};
            end
        elseif strcmp(varargin{place},'accuracy')
            if any(validatestring(varargin{place+1},{'low','medium','high','vhigh'}));
                accuracy = varargin{place+1};
            end
        end
    end
end

%Check if degree is between 0 and 5
if degree > 5 || degree < 0
    error('MATLAB:ParamaterNotInRange','degree must be an integer between 0 and 5');
end

%Figure out how to express v
if isa(v_input,'double') %The input is a vector of points at each %x_i of 
                         %the mesh
    if size(v_input) == size(p) %Check to see if the sizes are the same
        v = @(x) interp1(p,v_input,x,'PCHIP'); %Cubic interpolation
    elseif all(size(v_input) == [number_of_basis,column_size-1]);
        %In this case v is previous derivative data
        v = v_input;
    else
        error('MATLAB:DimensionMismatch','The size of the mesh vector and point vector are not the same.');
    end
elseif isa(v_input,'function_handle')
    v = v_input;
else
    v = str2func(v_input);
end

%Create Polynomial Values
[ polynomial_values, polynomial_int_values, polynomial_deriv_values ] = createPolyValues( degree,accuracy );

%Create Edge Values
edge_values = createEdgeValues( v, p, polynomial_values );
%Define a cell containing the polynomial on each element
poly_data = zeros(number_of_basis,column_size-1);
add_data = zeros(number_of_basis,column_size-1);
for i = 1:(column_size-1)
    v_at_a = edge_values(:,i);
    v_at_b = edge_values(:,i+1);
    if strcmp(derivative_type,'both')
        [element_poly_plus, element_poly_minus] = numericalDerivative(v,degree,p(i),p(i+1),p, ...
            i,column_size-1,...
            derivative_type, accuracy,...
            polynomial_values, polynomial_int_values, polynomial_deriv_values,...
            v_at_a, v_at_b);
        poly_data(:,i) = element_poly_plus;
        add_data(:,i) = element_poly_minus;
    else
        element_poly = numericalDerivative(v,degree,p(i),p(i+1),p, ...
            i,column_size-1,...
            derivative_type, accuracy,...
            polynomial_values, polynomial_int_values, polynomial_deriv_values,...
            v_at_a, v_at_b);
        poly_data(:,i) = element_poly;
    end
end


end

