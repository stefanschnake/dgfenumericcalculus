function [ point_matrix, weight_vector ] = createPointWeightVectors( accuracy )
%Function to get the point and weight vectors for the Gaussian Quadrature determined by the accuracy 

%%% AUTHOR: Stefan Schnake, 2014

%Create the point vector and wight vector.
if strcmp(accuracy,'low')
    point_matrix = [-(1/3)^(1/2),(1/3)^(1/2)];
    weight_vector = [1,1];
elseif strcmp(accuracy,'medium')
    point_matrix =  [(3/5)^(1/2),0,-(3/5)^(1/2)];
    weight_vector = [5/9,8/9,5/9];
elseif strcmp(accuracy,'high');
    point_matrix = [0,(1/3)*(5-2*(10/7)^(1/2))^(1/2), ...
        -(1/3)*(5-2*(10/7)^(1/2))^(1/2), ...
        (1/3)*(5+2*(10/7)^(1/2))^(1/2),-(1/3)*(5+2*(10/7)^(1/2))^(1/2)];
    weight_vector = [128/225,(322+13*70^(1/2))/900,...
        (322+13*70^(1/2))/900,(322-13*70^(1/2))/900,...
        (322-13*70^(1/2))/900];
elseif strcmp(accuracy,'vhigh')
    matt = ... 
[0.4179591836734694	0.0000000000000000;
0.3818300505051189	0.4058451513773972;
0.3818300505051189	-0.4058451513773972;
0.2797053914892766	-0.7415311855993945;
0.2797053914892766	0.7415311855993945;
0.1294849661688697	-0.9491079123427585;
0.1294849661688697	0.9491079123427585];
point_matrix = matt(:,2)';
weight_vector = matt(:,1)';
end


end

