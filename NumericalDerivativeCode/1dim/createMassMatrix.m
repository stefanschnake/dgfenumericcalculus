function [ mass_matrix ] = createMassMatrix( degree )
%Creates the mass matrix for numerical derivative.  Since this is the one
%dimension case, we assume that our domain is (0,1).  For basis polynomials
% of degree less than or equal to the input, compute the integral of their
%product.

%%% AUTHOR: Stefan Schnake, 2014

number_of_basis = nchoosek(1+degree,degree);

%Initalize Mass Matrix
mass_matrix = zeros(number_of_basis);

%Create Polynomial Basis.
polynomial_basis = zeros(6,6);
polynomial_derivative_basis = zeros(6,6);
polynomial_basis(1,:) =  (1/2)^(1/2)*[0 0 0 0 0 1];
polynomial_basis(2,:) =  (3/2)^(1/2)*[0 0 0 0 1 0];
polynomial_basis(3,:) =  (5/2)^(1/2)*(1/2)*[0 0 0 3 0 -1];
polynomial_basis(4,:) =  (7/2)^(1/2)*(1/2)*[0 0 5 0 -3 0];
polynomial_basis(5,:) =  (9/2)^(1/2)*(1/8)*[0 35 0 -30 0 3];
polynomial_basis(6,:) = (11/2)^(1/2)*(1/8)*[63 0 -70 0 15 0];

polynomial_derivative_basis(1,:) = zeros(1,6);
for j=2:6
    polynomial_derivative_basis(j,:) = [zeros(1,6-(j-1)) polyder(polynomial_basis(j,:))];
end
save('polynomial_basis.mat','polynomial_basis');
save('polynomial_derivative_basis.mat','polynomial_derivative_basis'); 

for j = 1:number_of_basis
    for k = 1:number_of_basis
        %Compute antiderivative of product of polys
        antiderivative = polyint(conv(polynomial_basis(j,:),polynomial_basis(k,:)));
        mass_matrix(j,k) = polyval(antiderivative,1) - polyval(antiderivative,-1);
    end
end






end

