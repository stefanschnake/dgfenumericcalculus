function [ polynomial_values, polynomial_int_values, polynomial_deriv_values ] = createPolyValues( degree,accuracy )

%%% AUTHOR: Stefan Schnake, 2014

number_of_basis = 1+degree;
[point_matrix, weight_vector] = createPointWeightVectors(accuracy);

%Create polynomial derivative and polynomial values for integration.
%Note that the Legendre Polynomials follow the recurrence relation
%P_{n+1}(x) = 1/(n+1) * ( (2n+1)*x*P_{n}(x) - n*P_{n-1}(x) )
%and the derivative follows
%P'_{n+1}(x) = 1/n * ( (2n+1)*x*P'{n}(x) - (n+1)*P'_{n-1}(x) )
%However in our case P starts with indexing 1 while in most literature the
%Legendre polys start with 0 so we have to be a bit careful.
polynomial_values = zeros(number_of_basis,2);
polynomial_deriv_values = zeros(number_of_basis,size(point_matrix,2));
polynomial_int_values = zeros(number_of_basis,size(point_matrix,2));
if number_of_basis == 1
    polynomial_values = [1 1];
    polynomial_int_values = ones(size(point_matrix));
elseif number_of_basis == 2
    polynomial_values = [1 1; -1 1];
    polynomial_int_values = [ones(size(point_matrix)); point_matrix];
    polynomial_deriv_values(2,:) = ones(size(point_matrix));
else
    polynomial_values(1:2,:) = [1 1; -1 1];
    polynomial_int_values(1:2,:) = [ones(size(point_matrix)); point_matrix];
    polynomial_deriv_values(2,:) = ones(size(point_matrix));
    for i = 3:number_of_basis
        n = i-1;
        polynomial_values(i,:) = (1/n) * ...
            ( (2*n-1)*[-1 1].*polynomial_values(i-1,:) - ...
            (n-1)*polynomial_values(i-2,:) );
        polynomial_int_values(i,:) = (1/n) * ...
            ( (2*n-1)*point_matrix.*polynomial_int_values(i-1,:) - ...
            (n-1)*polynomial_int_values(i-2,:) );
        polynomial_deriv_values(i,:) = (1/(n-1)) * ...
            ( (2*n-1)*point_matrix.*polynomial_deriv_values(i-1,:) - ...
            n*polynomial_deriv_values(i-2,:) );
    end
end
end

