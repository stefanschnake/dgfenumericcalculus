function [ z ] = postProcessing( poly_data,p,format_option,x)
%Allows the user to plot the numerical derivative given its data and some formatting options.

%%% AUTHOR: Stefan Schnake, 2014

%Get the size of x to reshape later
[reshape_row_length, reshape_column_length] = size(x);
column_size = size(p,2);

%If multivalued output is selected, x needs to be a row vector
if format_option && (reshape_row_length ~= 1)
    error('MATLAB:DimensionMismatch','x needs to be a row vector for multivalued output');
end
x = x(:);
[x,perm] = sort(x);

element_array = cell(2,column_size);
elements = logical(zeros(size(x,1),column_size-1));
%Organizing elements by their column.
for j = 1:column_size-1
    elements(:,j) = (p(j) <= x) & (x <= p(j+1)); 
end
sum_values = 0;
for i = 1:size(x,1)
    indicies = find(elements(i,:));
    for k = 1:size(indicies,2)
        sum_values = sum_values + 1;
        element_array{1,indicies(k)} = [element_array{1,indicies(k)} x(i)];
        element_array{2,indicies(k)} = [element_array{2,indicies(k)} i];
    end
end
%Create local affine transformation on each column that maps to [-1 1] and
%apply it to each the x values the column
affine_point_index = zeros(1,sum_values);
affine_x_values = zeros(1,sum_values);
element_value = zeros(1,sum_values);
temp_counter = 1;
for j=1:column_size-1
    position = size(element_array{1,j},2);
    if position > 0
        affine_x_values(temp_counter:(temp_counter+position-1)) = (2/(p(j+1)-p(j)))*(element_array{1,j}-(p(j)+p(j+1))/2);
        affine_point_index(temp_counter:(temp_counter+position-1)) = element_array{2,j};
        element_value(temp_counter:(temp_counter+position-1)) = j*ones(1,position);
        temp_counter = temp_counter + position;
    end
end

%Recursively compute the legendre polynomial for each affine value given
number_of_basis = size(poly_data,1);
polynomial_values = zeros(number_of_basis,size(affine_x_values,2));
if number_of_basis == 1
    polynomial_values = ones(size(affine_x_values));
elseif number_of_basis == 2
    polynomial_values = [ones(size(affine_x_values)); affine_x_values];
else
    polynomial_values(1:2,:) = [ones(size(affine_x_values)); affine_x_values];
    for i = 3:number_of_basis
        n = i-1;
        polynomial_values(i,:) = (1/n) * ...
            ( (2*n-1)*affine_x_values.*polynomial_values(i-1,:) - ...
            (n-1)*polynomial_values(i-2,:) );
    end
end
%Create the actual values by dotting the poly output with it's basis vector
temp_fun_values = zeros(1,sum_values);
for j = 1:sum_values
    temp_fun_values(j) = dot(poly_data(:,element_value(j)),polynomial_values(:,j));
end
if format_option
    z = zeros(3,size(x,1));
    sorted_z = zeros(3,size(x,1));
else
    z = zeros(size(x'));
    sorted_z = zeros(size(x'));
end
count = 1;
for i = 1:size(x,1)
    if count <= (size(affine_point_index,2)-1) && affine_point_index(count + 1) == i
        indicies = [count count+1];
        count = count + 2;
    else
        indicies = count;
        count = count + 1;
    end
    if size(indicies,2) == 2
        y = [temp_fun_values(indicies(1)),temp_fun_values(indicies(2))];
        if format_option
            z(:,i) = [y(2);y(1);(y(1)+y(2))/2];
        else
            z(:,i) = (y(1)+y(2))/2;
        end
    else
        y = temp_fun_values(indicies);
        if format_option
            z(:,i) = [y;y;y];
        else
            z(:,i) = y;
        end
    end 
end
for i = 1:size(z,2)
    sorted_z(:,perm(i)) = z(:,i);
end
z = sorted_z;
%reshape z
if format_option == 0
    z = reshape(z,reshape_row_length,reshape_column_length);
end
end

