function [ edge_data ] = createEdgeData( point_matrix, edge_matrix, triangle_matrix, triangle )
%Function to return edgedata from a particular triangle 

%%% AUTHOR: Stefan Schnake, 2014

%The edge data is as follows---------------------
% | 1 first point of edge                         |
% | 2 second point of edge                        |
% | 3 triangle # of the edges shared partner      |
% | 4 x-coordinate of unit outward normal vector  |
% | 5 y-coordinate of unit outward normal vector  |
% | 6 element normal vector == edge normal vector |
% |   is the edge on the boundary of the mesh     |
%------------------------------------------------


N = size(triangle_matrix,2);
%Vector that saves which triangles are next to the inputed triangle
bordering_triangles = [];
edge_data = [];
triangle_data = triangle_matrix(1:3,triangle);

%Find which triangles border the inputed triangle (this will give us all
%the edges except the ones on the boundary)
for i=[1:(triangle-1),(triangle+1):N]
        temp_triangle = triangle_matrix(1:3,i);
        M1 = size(temp_triangle,1);
        %Keeps track of the number of points shared with the inputed triangle
        common_point_count = 0;
        temp_point_vector = [];
        for j = 1:M1
            %Raise the count by 1 if the point is in the inputed triangles
            %point list
            %[temp_triangle(j), ismember(temp_triangle(j),triangle_data)]
            temp = temp_triangle(j);
            if (temp == triangle_data(1)) || (temp == triangle_data(2)) || (temp == triangle_data(3))
                common_point_count = common_point_count + 1;
                temp_point_vector = [temp_point_vector; temp_triangle(j)];
            end
        end
        %A point count of 2 means they share the same edge
        if common_point_count == 2;
            bordering_triangles = [bordering_triangles i]; 
            edge_data = [edge_data [temp_point_vector;i;0;0;0;0]];
        end
end

%Now we must find the edge vectors
N = size(edge_matrix,2);
for i = 1:N
    temp_edge = edge_matrix(1:2,i);
    edge_count = 0;
    for j = 1:size(triangle_data)
        if sum(triangle_data(j) == temp_edge) > 0
            edge_count = edge_count + 1;
        end
    end
    if edge_count == 2 %thus we have an edge
        edge_data = [edge_data [temp_edge ;0;0;0;0;1]];
    end
end


%Get the rest of the edge_data once we have the edges.  This includes the
%unit outward normal vector in respect to K and unit normal edge vector 
%NOTE: RIGHT NOW THIS ONLY WORKS FOR TRIANGLES AND OTHER CONVEX ELEMENTS
[M,N] = size(edge_data);
for i = 1:N
    edge_point_1 = point_matrix(:,edge_data(1,i)); %(x_1,x_2)
    edge_point_2 = point_matrix(:,edge_data(2,i)); %(y_1,y_2)
    %Get last point
    for j = 1:3
        if sum(triangle_data(j) == edge_data(1:2,i)) == 0
            non_edge_point = point_matrix(:,triangle_data(j));
            break
        end
    end
    
    %Create normal vector
    normal_vector = getNormalVector(edge_point_1,edge_point_2,non_edge_point);
   
    %Place the normal vector in the edge_data
    edge_data(M-3:M-2,i) = normal_vector';
    
    %Finally we must compute the normal edge vector which is an outward
    %vector but points in the direction of the largest element. We will
    %store a boolean value of 1 if the normal vector is correct and 0 if it
    %is pointing the opposite direction.  We only do this if the edge is not on the
    %boundary
    if edge_data(M,i) == 0
        edge_data(M-1,i) = (bordering_triangles(i) > triangle);
    else
        edge_data(M-1,i) = 1;
    end
end


end

