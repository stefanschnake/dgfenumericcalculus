function [ z ] = getBoundaryFunctionValues( s, ...
    edge_point_1, edge_point_2,v)
%This funciton calculates the boundary values needed for the one dimension
%edge integral given a function v defined on the line connecting edge
%point_1 and edge_point_2 

%%% AUTHOR: Stefan Schnake, 2014

N = size(s,2);
temp_xy = zeros(2,N);
for i = 1:N
    temp_xy(:,i) = (edge_point_1+edge_point_2)/2 + s(i)*(edge_point_2-edge_point_1)/2;
end

temp_x = temp_xy(1,:)';temp_y = temp_xy(2,:)';

z = v(temp_x,temp_y)';

end

