function [ plus_b_vector, minus_b_vector ] = createRightHandSide( original_v, triangle_matrix,...
    point_matrix, point_1, point_2, point_3, affine_matrix, ...
    affine_matrix_inverse, determinant, ...
    edge_data, triangle, polynomial_basis_order, degree, position, accuracy)
%Create right hand side (plus and minus) v on the element K with label
%'triangle'
%triangle_matrix and point matrix come from the mesh
%triangle is the number of the triangle in triangle_matrix
%degree is the polynomial degree
%position is the marker for what partial derivative we are taking

%%% AUTHOR: Stefan Schnake, 2014

%Create the total number of basis elements
number_of_basis = size(polynomial_basis_order,1);

%Use the accuracy parameter to determine what order of quadrature the
%program will use.
if strcmp(accuracy,'low')
    interior_point_vector =  [1/6,2/3,1/6; ...
                      1/6,1/6,2/3];
    interior_weight_vector = [1/6,1/6,1/6];
    edge_point_vector = [-(1/3)^(1/2),(1/3)^(1/2)];
    edge_weight_vector = [1,1];
elseif strcmp(accuracy,'medium')
    interior_point_vector = [1/3,0.2,0.2,0.6; ...
                      1/3,0.6,0.2,0.2];
    interior_weight_vector = [-9/32,25/96,25/96,25/96];
    edge_point_vector =  [(3/5)^(1/2),0,-(3/5)^(1/2)];
    edge_weight_vector = [5/9,8/9,5/9];
elseif strcmp(accuracy,'high') 
    interior_point_vector = ...
       [0.333333333333330   0.470142064105110 ...
        0.470142064105110   0.059715871789770 ...
        0.101286507323460   0.101286507323460 ...
        0.797426985353090 ;
        0.333333333333330   0.470142064105110 ...
        0.059715871789770   0.470142064105110 ...
        0.101286507323460   0.797426985353090 ...
        0.101286507323460];
    interior_weight_vector = [0.225000000000000   0.132394152788510 ...
        0.132394152788510     0.132394152788510   0.125939180544830 ...
        0.125939180544830     0.125939180544830]/2;
    edge_point_vector = [0,(1/3)*(5-2*(10/7)^(1/2))^(1/2),...
        -(1/3)*(5-2*(10/7)^(1/2))^(1/2), ...
       (1/3)*(5+2*(10/7)^(1/2))^(1/2),-(1/3)*(5+2*(10/7)^(1/2))^(1/2)];
    edge_weight_vector = [128/225,(322+13*70^(1/2))/900,...
        (322+13*70^(1/2))/900, ...
       (322-13*70^(1/2))/900,(322-13*70^(1/2))/900];
else
    interior_point_vector = [0.33333333333333 0.33333333333333;
    0.26034596607904 0.26034596607904;
    0.26034596607904 0.47930806784192;
    0.47930806784192 0.26034596607904;
    0.06513010290222 0.06513010290222;
    0.06513010290222 0.86973979419557;
    0.86973979419557 0.06513010290222;
    0.31286549600487 0.63844418856981;
    0.63844418856981 0.04869031542532;
    0.04869031542532 0.31286549600487;
    0.63844418856981 0.31286549600487;
    0.31286549600487 0.04869031542532;
    0.04869031542532 0.63844418856981]';

    interior_weight_vector = ...
        [-0.14957004446768 0.17561525743321 ...
          0.17561525743321 0.17561525743321 ...
          0.05334723560884 0.05334723560884 ...
          0.05334723560884 0.07711376089026 ...
          0.07711376089026 0.07711376089026 ...
          0.07711376089026 0.07711376089026 ...
          0.07711376089026]/2;
  
  
    edge_point_vector = [0,(1/3)*(5-2*(10/7)^(1/2))^(1/2),...
        -(1/3)*(5-2*(10/7)^(1/2))^(1/2), ...
       (1/3)*(5+2*(10/7)^(1/2))^(1/2),...
       -(1/3)*(5+2*(10/7)^(1/2))^(1/2)];
    edge_weight_vector = [128/225,(322+13*70^(1/2))/900,...
        (322+13*70^(1/2))/900, ...
       (322-13*70^(1/2))/900,(322-13*70^(1/2))/900];           
end

    %Define a function that evaluates v at the affine transformation
    function z = affine_v(x,y,fun,matrix,point)
        [temp_x, temp_y] = getAffineMapping(x,y,matrix,point);
        z = fun(temp_x,temp_y);
    end

    function z = affine_inv_v(x,y,fun,matrix,point)
        [temp_x,temp_y] = getAffineMappingInverse(x,y,matrix,point);
        z = fun(temp_x,temp_y);
    end

%Next we must make sure v is properly defined
if isa(original_v,'double')
    %In this case our v is still the previous derivative data so must
    %define v properly on the interior of our element.
    poly_v = @(x,y) polyval2(original_v(:,triangle)',x,y);
    v = @(x,y) affine_inv_v(x,y,poly_v,...
                affine_matrix_inverse,point_1);
else
    v = original_v;
end

%These are the values of v that will be used in the 2d integration.  Since
%we are in the interior of v is multivalued then the values must be the
%same - so we will only use the top values.
%Note that if original v is previous derivative data, is it already defined
%on the simplex, so there is no need for the affine transformation.  If
%orginal_v is defined on K, then we need to create the affine
%transformation.
if isa(original_v,'double')
    temp_v_values = poly_v(interior_point_vector(1,:),interior_point_vector(2,:));
else
    temp_v_values = affine_v(interior_point_vector(1,:)',...
        interior_point_vector(2,:)',v,affine_matrix,point_1)';
end
if size(temp_v_values,1) == 2
    v_values = temp_v_values(1,:);
else
    v_values = temp_v_values;
end

%Create the boundary value vectors for the edge integration
%edge_size_N is the number of edges on the element labled 'triangle'.  I am
%assuming the elements are triangles so this will be always 3 but I hope to
%expand this code it tetrahedral elements.
edge_size_N = size(edge_data,2);
%These two vectors will hold the v values used for the edge integration.
%If v is multivalued on the edge the plus and minus vectors will be
%different.
plus_v_edge_values = zeros(edge_size_N,size(edge_point_vector,2));
minus_v_edge_values = zeros(edge_size_N,size(edge_point_vector,2));
affine_boundary_points = cell(edge_size_N,1);
%Create a vector to hold the constants applied by the integration
%substitution.
integration_modifier = zeros(edge_size_N,1);

%Next we must compute the values of v used for the edge integration.  We
%must be careful here as v is multivalued on the edge with two entries.
%The order of the entires is with respect to the global element index of
%the two elements sharing the edge.
for k = 1:edge_size_N

    %Get the required points and other needed values from the edge_data.
    edge_point_1 = point_matrix(:,edge_data(1,k));
    edge_point_2 = point_matrix(:,edge_data(2,k));
    neighboring_triangle = edge_data(3,k);
    normal_vector = edge_data(4:5,k);
    edge_vector = (-1)^(edge_data(6,k)+1)*normal_vector;
    
    %Next get the output of v on the edge at the specified points.
    if isa(original_v,'double')
        %If v holds old derivative data.  Then we must order the outputs
        %ourselves.  First, if the edge in question is on the edge of the
        %mesh, then neighboring_triangle will be 0.
        if neighboring_triangle
            %Now we need the affine mapping the the neighboring triangle
            [neighbor_matrix, neighbor_point_1] = getAffineMatrix(point_matrix,...
                triangle_matrix,neighboring_triangle);
            neighbor_inverse = inv(neighbor_matrix);
            neighboring_v = @(x,y) polyval2(...
                original_v(:,neighboring_triangle)',x,y);
            affine_neighbor_v = @(x,y) affine_inv_v(x,y,neighboring_v,...
                neighbor_inverse,neighbor_point_1);
            neighboring_edge_outputs = getBoundaryFunctionValues(...
                edge_point_vector,edge_point_1,edge_point_2,...
                affine_neighbor_v);
            triangle_edge_outputs = getBoundaryFunctionValues(...
                edge_point_vector,edge_point_1,edge_point_2,v);
            if triangle > neighboring_triangle
                %In this case neighboring triangle will be the element on
                %top
                temp_edge_outputs = [neighboring_edge_outputs;triangle_edge_outputs];
            else
                temp_edge_outputs = [triangle_edge_outputs;neighboring_edge_outputs];
            end
        else
            temp_edge_outputs = getBoundaryFunctionValues(edge_point_vector,...
            edge_point_1,edge_point_2,v);
        end
    else
        temp_edge_outputs = getBoundaryFunctionValues(edge_point_vector,...
            edge_point_1,edge_point_2,v);
    end
    
    %Check the size of the output vector
    if size(temp_edge_outputs,1) == 1
        %If this triggers then v is continuous across the edge - hence the
        %plus and minus vectors are the same
        plus_v_edge_values(k,:) = temp_edge_outputs;
        minus_v_edge_values(k,:) = temp_edge_outputs;
    else
        %In this case the outputs have two values.  First create the
        %jump and average vectors
        jump_of_v = temp_edge_outputs(1,:) - temp_edge_outputs(2,:);
        average_of_v = (temp_edge_outputs(1,:) + temp_edge_outputs(2,:))/2;
        %Now fix the plus and minus values
        plus_v_edge_values(k,:) = (average_of_v ...
            + (1/2)*sign(edge_vector(position))*jump_of_v);
        minus_v_edge_values(k,:) = (average_of_v ...
            - (1/2)*sign(edge_vector(position))*jump_of_v);
    end
        
    %Set other edge integration vectors.  Here the normal vector comes from
    %the integration by parts formulation of the numerical derivative and
    %the (1/2)*norm(...) comes from the paramterization from a boundary
    %integral to a 1 dimensional integral.
    integration_modifier(k) = (1/2)*norm(edge_point_2-edge_point_1) ...
        *normal_vector(position);
    %Here affine_boundary_points holds the x and y values after the affine
    %transformation that wil be used with the polynomial.
    affine_boundary_points{k} = createAffineEdgePoints(edge_point_vector, ...
        affine_matrix_inverse,edge_point_1,edge_point_2,point_1);
end

%Compute all the values we need for the polyonmials and their powers
poly_order_inverse = ...
    [01 03 06 10 15;...
     02 05 09 14 00;...
     04 08 13 00 00;...
     07 12 00 00 00;...
     11 00 00 00 00];
powers = cell(number_of_basis,1);
x1 = [interior_point_vector(1,:) ...
      affine_boundary_points{1}(:,1)' ...
      affine_boundary_points{2}(:,1)' ...
      affine_boundary_points{3}(:,1)'];
y1 = [interior_point_vector(2,:) ...
      affine_boundary_points{1}(:,2)' ...
      affine_boundary_points{2}(:,2)' ...
      affine_boundary_points{3}(:,2)'];
for i=1:degree+1
    for j = 1:degree+1
        index = poly_order_inverse(i,j);
        if index > 0
            if index == 1
                powers{index} = ones(size(x1));
            elseif index == 2      
                powers{index} = x1;
            elseif index == 3
                powers{index} = y1;
            elseif i == 1
                index2 = poly_order_inverse(i,j-1);
                powers{index} = powers{index2}.*powers{3};
            elseif j == 1
                index2 = poly_order_inverse(i-1,j);
                powers{index} = powers{index2}.*powers{2};
            else
                index2 = poly_order_inverse(i,1);
                index3 = poly_order_inverse(1,j);
                powers{index} = powers{index2}.*powers{index3};
            end
        end
    end
end
%Now that we have the powers we can construct the polynomials and
%the polynomial partials in construction with the basis.

%We only need the polynomial values at the edge values
num_edge_points = edge_size_N*size(edge_point_vector,2);
num_int_points = size(interior_point_vector,2);
polynomial_values = zeros(number_of_basis, num_edge_points);
for i = 1:number_of_basis
    polynomial_values(i,:) ...
        = powers{i}(num_int_points+1:end);
end

%The partial values are computed on the interior.  Note that the affine
%matrix inverse is there for the gradient which is what we need because of
%the affine transformation.
partial_x = zeros(number_of_basis, num_int_points);
for i = 1:number_of_basis
    x_pow = polynomial_basis_order(i,1);
    y_pow = polynomial_basis_order(i,2);
    if x_pow == 0
        partial_x(i,:) = zeros(1,num_int_points);
    else
        partial_x(i,:) = x_pow *affine_matrix_inverse(1,position)...
            * powers{poly_order_inverse(x_pow,y_pow+1)}(1:num_int_points);
    end
end
partial_y = zeros(number_of_basis, num_int_points);
for i = 1:number_of_basis
    x_pow = polynomial_basis_order(i,1);
    y_pow = polynomial_basis_order(i,2);
    if y_pow == 0
        partial_y(i,:) = zeros(1,num_int_points);
    else
        partial_y(i,:) = y_pow * affine_matrix_inverse(2,position)...
            * powers{poly_order_inverse(x_pow+1,y_pow)}(1:num_int_points);
    end
end
gradient = partial_x + partial_y;

%Now we can complete the integration.
interior_weight_matrix = repmat(interior_weight_vector,number_of_basis,1);
edge_weight_matrix = repmat(edge_weight_vector,number_of_basis,1);
v_matrix = repmat(v_values,number_of_basis,1);
interior_integral = -abs(determinant)*...
    sum(interior_weight_matrix.*gradient.*v_matrix,2);
plus_edge_integral = zeros(number_of_basis,1);
minus_edge_integral = zeros(number_of_basis,1);
for k = 1:edge_size_N
    edge_size = size(edge_weight_vector,2);
    poly_values = polynomial_values(:,(k-1)*edge_size+1:k*edge_size);
    sum( edge_weight_matrix.*poly_values ...
        .* repmat(plus_v_edge_values(k,:),number_of_basis,1),2);
    plus_edge_integral = plus_edge_integral + integration_modifier(k)...
        * sum( edge_weight_matrix.*poly_values ...
        .* repmat(plus_v_edge_values(k,:),number_of_basis,1),2);
    minus_edge_integral = minus_edge_integral + integration_modifier(k)...
        * sum( edge_weight_matrix.*poly_values ...
        .* repmat(minus_v_edge_values(k,:),number_of_basis,1),2);
end
plus_b_vector = interior_integral + plus_edge_integral;
minus_b_vector = interior_integral + minus_edge_integral;

end

