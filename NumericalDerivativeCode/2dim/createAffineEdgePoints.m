function [ affine_points ] = createAffineEdgePoints( s, affine_inv_matrix, ...
    edge_point_1, edge_point_2, point)
%Calculate the affine transformation of the points needed for the edge integration

%%% AUTHOR: Stefan Schnake, 2014

N = size(s,2);
temp_xy = zeros(2,N);
for i = 1:N
    temp_xy(:,i) = (edge_point_1+edge_point_2)/2 + s(i)*(edge_point_2-edge_point_1)/2;
end
temp_x = temp_xy(1,:)';temp_y = temp_xy(2,:)';

[affine_x,affine_y] = getAffineMappingInverse(temp_x,temp_y, ...
    affine_inv_matrix,point);

affine_points = [affine_x, affine_y];

end

