function [ point_matrix, edge_matrix, edge_data, triangle_matrix ] = createMesh( x0, x1, y0, y1, N )
%Create triangular mesh
%N = number of trianges on a bottow row h = 1/(N-1)

point_matrix = zeros(2,N^2);
triangle_matrix = zeros(4,2*(N-1)^2);
edge_data = cell(1,2*(N-1)^2);
edge_matrix = [];

%Create points
for i=1:N %yval
    for j=1:N %xval
        place = (i-1)*N+j;
        xval = x0 + (x1-x0)*(j-1)/(N-1);
        yval = y0 + (y1-y0)*(i-1)/(N-1);
        point_matrix(:,place) = [xval;yval];
    end
end

tempedgedata = zeros(7,3);
%Create triangle/edge matrix
for i=1:N-1 %yrow
    for j=1:N-1 %xrow
        for k=1:2 %two positions
            place = 2*((i-1)*(N-1)+j)+(k-2);
            if k==1
                point1 = (i-1)*N+j;
                point2 = point1 + 1;
                point3 = point1 + N;
                
                %Edge data
                tempedgedata = ...
                    [point1         , point2   , point3 ; ...
                     point2         , point3   , point1 ; ...
                     place-2*(N-1)+1, place+1  , place-1; ...
                     0              , 1/sqrt(2), -1     ; ...
                     -1             , 1/sqrt(2),  0     ; ...
                     0              , 1        ,  0     ; ...
                     0              , 0        ,  0      ];
                 
                 if i==1
                     tempedgedata(3,1) = 0;
                     tempedgedata(6,1) = 1;
                     tempedgedata(7,1) = 1;
                 end
                 if j==1
                     tempedgedata(3,3) = 0;
                     tempedgedata(6,3) = 1;
                     tempedgedata(7,3) = 1;
                 end   
            else
                point1 = i*N+(j+1);
                point2 = point1 - 1;
                point3 = point1 - N;
                
                %Edge data
                tempedgedata = ...
                    [point1         , point2   , point3 ; ...
                     point2         , point3   , point1 ; ...
                     place+2*(N-1)-1, place-1  , place+1; ...
                     0              ,-1/sqrt(2),  1     ; ...
                     1              ,-1/sqrt(2),  0     ; ...
                     1              , 0        ,  1     ; ...
                     0              , 0        ,  0      ];
                 
                 if i==N-1
                     tempedgedata(3,1) = 0;
                     tempedgedata(6,1) = 1;
                     tempedgedata(7,1) = 1;                     
                 end
                 if j==N-1
                     tempedgedata(3,3) = 0;
                     tempedgedata(6,3) = 1;
                     tempedgedata(7,3) = 1;                     
                 end
            end
            triangle_matrix(:,place) = [point1;point2;point3;1];
            edge_data{1,place} = tempedgedata;
            
            %Create edges
            if (i==1) && (k==1)
                edge_matrix = [edge_matrix [point1;point2]];
            end
            if (i==N-1) && (k==2)
                edge_matrix = [edge_matrix [point1;point2]];
            end
            if (j==1) && (k==1)
                edge_matrix = [edge_matrix [point1;point3]];                
            end
            if (j==N-1) && (k==2)
                edge_matrix = [edge_matrix [point1;point3]];                
            end
        end
    end
end


end

