function [ mass_matrix, polynomial_basis_order ] = createMassMatrix( degree )
%Creates the mass matrix for numerical derivative. For basis polynomials
% of degree less than or equal to the input, compute the integral of their
%product.


%%% AUTHOR: Stefan Schnake, 2014

number_of_basis = nchoosek(2+degree,degree);

%Initalize Mass Matrix
mass_matrix = zeros(number_of_basis);

%Create Polynomial Basis.  We will use a cell array approach where the
%first column is the function handle and the second column is a row vector
%[i,j] where i and j are the degrees and x and y respectively.
polynomial_basis = cell(number_of_basis,1);
polynomial_basis_order = zeros(number_of_basis,2);
%Setting the first polymial as the constant 1
polynomial_basis{1} = @(x,y) ones(size(x));
polynomial_basis_order(1,:) = [0,0];

%This part of the code could be optimized but we are only dealing with a
%20x20 matrix maximum so this implementation will work for now.
count = 0;
for i = 0:degree
    for j=0:degree
        for k=0:degree
            if (j+k) == i
                count = count + 1;
                polynomial_basis{count} = @(x,y) (x.^k).*(y.^j);
                polynomial_basis_order(count,:) = [k,j];
            end
        end
    end
end

ymax = @(x) 1-x;
for j = 1:number_of_basis
    for k = 1:number_of_basis
        %Compute integral of product of basis polynomials
        integrating_function = @(x,y) polynomial_basis{j}(x,y).*polynomial_basis{k}(x,y);
        mass_matrix(j,k) = integral2(integrating_function,0,1,0,ymax);
    end
end

end

