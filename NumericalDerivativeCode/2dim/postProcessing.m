function [ z ] = postProcessing(poly_data, ...
    point_matrix, triangle_matrix, format_option, x, y)
%Use polyval2 provided to evaluate the 2d polynomial given the X and Y,
%represented as vectors 

%%% AUTHOR: Stefan Schnake, 2014

%Shape x and y to column vectors and keep the original shape so we can
%reshape later.
%If format_option is set to multivalued (1) we need to make sure the input
%is a row vector, i.e., the row size is 1.
[reshape_row_size, reshape_column_size] = size(x);
if (reshape_row_size ~= 1) && format_option
    error('MATLAB:InvalidDeminsions','x and y must be row vectors in order to use the multivalued option.')
elseif any(size(x) == size(y)) == 0
    error('MATLAB:InvalidDeminsions','x and y must be the same dimensions.')
end
x = x(:);
y = y(:);
if format_option
    z = zeros(size(x,1),3);
else
    z = zeros(size(x));
end

%First we need to know where the points are on the mesh.
simplex_cell = simplexFinder( point_matrix, ...
    triangle_matrix, x, y);

%Now we must loop through each simplex of each point and calculate needed
%values.
for i = 1:size(simplex_cell,1)
    %triangles is a row vector containing the simplexes the point is in.
    %Edges contain two and vertices contain more than two BUT we limit the
    %size of the vector to two for simplicity. 
    triangles = simplex_cell{i};
    temp_values = zeros(1,size(triangles,2));
    for j = 1:size(triangles,2)
        %Before we compute the value of the function.  (x,y) must be mapped
        %to thier relative point on the simplex domain.
        [A, point_1]= getAffineMatrix(point_matrix,triangle_matrix,triangles(j));
        inverse_matrix = 1/(A(1,1)*A(2,2)-A(1,2)*A(2,1))*[A(2,2) -A(1,2);-A(2,1) A(1,1)];
        [affine_x,affine_y] = getAffineMappingInverse(x(i),y(i),inverse_matrix,point_1);
        temp_values(j) = polyval2(poly_data(:,triangles(j))',affine_x,affine_y);
    end
    if format_option
        z(i,:) = [temp_values(1),temp_values(end),mean(temp_values)];
    else
        z(i) = mean(temp_values);
    end
end

%Now we must reshape our z vector back to the way we need it
if format_option
    z = z';
else
    z = reshape(z,reshape_row_size, reshape_column_size);

end

