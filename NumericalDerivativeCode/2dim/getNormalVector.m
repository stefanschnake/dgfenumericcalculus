function [ normal_vector ] = getNormalVector( edge_point_1, edge_point_2, non_edge_point )
%Computes normal vector

%%% AUTHOR: Stefan Schnake, 2014

%edge_point_1 = (x_1,x_2)
%edge_point_2 = (y_1,y_2)
%Determine slope (y_2 - x_2)/(y_1 - x_1) 

err_tol = 10^(-13);

%if (y_1 - x_1)=0 then we have a vertical line, so the normal vector is
%[1,0]
rise = edge_point_2(2) - edge_point_1(2);
run = edge_point_2(1) - edge_point_1(1);
if abs(run) < err_tol
    normal_vector = [1;0];
%If (y_2 - x_2)=0 then we have a horizontal line, so the normal vector
%is [0,1]
elseif abs(rise) < err_tol
    normal_vector = [0;1];
%Else compute the recirpocal and normalize the vector
else
    non_normalized_normal_vector = [-rise;run];
    normal_vector = non_normalized_normal_vector ...
        /norm(non_normalized_normal_vector);
end
%Now that we have the normal vector we must make sure it is facing the
%outward direction.
forward_distance = norm(edge_point_1+normal_vector-non_edge_point);
backward_distance = norm(edge_point_1-normal_vector-non_edge_point);
%edge_point_distance = norm(edge_point_1-non_edge_point);
%new_distance = norm(edge_point_1+(normal_vector)'-non_edge_point);

%[edge_point_distance, new_distance]
%If the new distance is less than the edge point distance the normal
%vector is pointing inside and we must flip the direction
if forward_distance < backward_distance
    normal_vector = -normal_vector;
end

end

