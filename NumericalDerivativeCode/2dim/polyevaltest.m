%Compute all the values we need for the polyonmials and their powers

%%% AUTHOR: Stefan Schnake, 2014

poly_order_inverse = ...
    [01 03 06 10 15;...
     02 05 09 14 00;...
     04 08 13 00 00;...
     07 12 00 00 00;...
     11 00 00 00 00];
degree = 4;
number_of_basis = nchoosek(degree+2,2);
powers = cell(number_of_basis+1,1);
x1 = [00 .2 .4 .6 .8 1];
y1 = [.1 .3 .5 .7 .9 1];
for i=1:degree+1
    for j = 1:degree+1
        index = poly_order_inverse(i,j);
        if index > 0
            if index == 1
                powers{index} = ones(size(x1));
            elseif index == 2      
                powers{index} = x1;
            elseif index == 3
                powers{index} = y1;
            elseif i == 1
                index2 = poly_order_inverse(i,j-1);
                powers{index} = powers{index2}.*powers{3};
            elseif j == 1
                index2 = poly_order_inverse(i-1,j);
                powers{index} = powers{index2}.*powers{2};
            else
                index2 = poly_order_inverse(i,1);
                index3 = poly_order_inverse(1,j);
                powers{index} = powers{index2}.*powers{index3};
            end
            disp([i-1 j-1 -0.0001 abs(powers{index}-(x1.^(i-1).*y1.^(j-1)))])
        end
    end
end

clearvars degree i j number_of_basis poly_order_inverse index index2
clearvars index3
