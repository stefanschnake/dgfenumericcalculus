function [ poly_data ] = ...
    elementNumericalDerivative( v, ...
    triangle_matrix, point_matrix, edge_data, triangle, ...
    mass_matrix_inverse, polynomial_basis_order, degree, position, derivative_type, accuracy )
%Create the numerical derivative on the element with label "triangle"

%%% AUTHOR: Stefan Schnake, 2014

[affine_matrix, point_1, point_2, point_3] = getAffineMatrix(point_matrix,triangle_matrix,triangle);
affine_matrix_inverse = inv(affine_matrix);
determinant = det(affine_matrix);

%Get Edge Data
%edge_data = createEdgeData(point_matrix, edge_matrix, ...
%    triangle_matrix, triangle);
triangle_edge_data = edge_data{triangle};
%Get Right Hand Side
[plus_vector, minus_vector] = createRightHandSide(v, triangle_matrix, point_matrix, ...
    point_1, point_2, point_3, affine_matrix, affine_matrix_inverse, ...
    determinant, triangle_edge_data, triangle, polynomial_basis_order, ...
    degree, position, accuracy);
if strcmp(derivative_type,'plus')
    right_hand_side = plus_vector;
elseif strcmp(derivative_type,'minus')
    right_hand_side = minus_vector;
elseif strcmp(derivative_type,'full')
    right_hand_side = (1/2)*(plus_vector + minus_vector);
else
    error('MATLAB:RequiredArguments','Please use plus, minus, or full for the derivative_type option');
end

%Solve matrix equation
poly_data = (1/determinant)*(mass_matrix_inverse * right_hand_side);
end

