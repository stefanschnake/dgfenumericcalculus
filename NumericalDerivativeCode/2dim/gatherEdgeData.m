function [ edge_data ] = gatherEdgeData( point_matrix, edge_matrix, triangle_matrix )
%Creates a cell that captures the data for every triangle

%%% AUTHOR: Stefan Schnake, 2014

N = size(triangle_matrix,2);
edge_data = cell(1,N);
for triangle = 1:N
   edge_data{triangle} = createEdgeData(point_matrix,edge_matrix,triangle_matrix,triangle);
end


end

