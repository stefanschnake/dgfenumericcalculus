function [ zx,zy ] = getAffineMappingInverse( x,y,matrix,point )
%This the the affine transformation from the actual element K to the 
%simplex element K'.  Defined as [x;y] -> A^{-1}([x;y] - [b1,b2]) where 
%A^{-1} is the inverse of the affine mapping matrix and [b1,b2] is the
%first point that appears in the triangle_matrix list.

%%% AUTHOR: Stefan Schnake, 2014

[M,N] = size(x);
zx = zeros(M,N);
zy = zeros(M,N);
for q = 1:M
    for r = 1:N
        tempz = matrix*([x(q,r);y(q,r)] - point);
        zx(q,r) = tempz(1);
        zy(q,r) = tempz(2);
    end
end

end

