%Loads a default mesh 

%%% AUTHOR: Stefan Schnake, 2014

disp('Loading square centered at (.5,.5) with side length 1');
load('point.mat');
disp('Loaded point matrix as variable "p"');
load('edge.mat');
disp('Loaded edge matrix as variable "e"');
load('triangle.mat');
disp('Loaded triangle matrix as variable "t"');
ee = gatherEdgeData(p,e,t);
disp('Created edge data as variable "ee"');
