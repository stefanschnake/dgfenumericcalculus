function [ simplex_cell ] = simplexFinder( point_matrix, ...
    triangle_matrix, x, y)

%%% AUTHOR: Stefan Schnake, 2014

%Here x and y are Nx1 column vectors that represent the x and y coordinate for
%the N points.  The ouput is a Nx1 cell where each cell entry contains a
%row vector corresponding to position of each simples that the
%corresponding (x,y) point lies in.

[M_x,N_x] = size(x);

number_of_triangles = size(triangle_matrix,2);
simplex_cell = cell(M_x,1);

tol = -sqrt(eps);

Y = [ones(size(point_matrix',1),1) point_matrix']; 
d = [ones(size(x)) x y];
for i = 1:number_of_triangles
    q = d / Y(triangle_matrix(1:3,i)',:); %Barycenters for triangle i
    logical_index = all(q > tol,2);	   %Store triangle if all the barycenters are positive
    if any(logical_index);
        for j = 1:size(logical_index,1)
            %Check which (x,y) are in the closure of K. Note that this test
            %can be passed a multitude of times if (x,y) is on the edge
            %(pass two times) or if (x,y) is a vertex (5,6,7 times).  To
            %make things simpler we will only take the first two passes.
            if logical_index(j) && (size(simplex_cell{j},2)<=1)
                simplex_cell{j} = [simplex_cell{j} i];
            end
        end
    end
end


end

