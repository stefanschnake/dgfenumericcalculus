function [ affine_matrix, point_1, point_2, point_3 ] = getAffineMatrix( point_matrix,...
    triangle_matrix,triangle)

%%% AUTHOR: Stefan Schnake, 2014

%Create affine mapping from K to simplex K'.
triangle_point_indicies = triangle_matrix(1:3,triangle);
point_1 = point_matrix(:,triangle_point_indicies(1));
point_2 = point_matrix(:,triangle_point_indicies(2));
point_3 = point_matrix(:,triangle_point_indicies(3));
affine_matrix = [point_2(1) - point_1(1), point_3(1) - point_1(1); ...
  point_2(2) - point_1(2),point_3(2) - point_1(2) ];


end

